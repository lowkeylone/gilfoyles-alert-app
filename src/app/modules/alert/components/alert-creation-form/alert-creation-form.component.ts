import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatStepper } from '@angular/material';
import { AppStore } from '@core/store';
import { AlertCreationFormStore, AlertsStore } from '@modules/alert/store';
import { Store } from '@ngrx/store';
import { RingtoneCreationDialogComponent } from '@shared/components';
import { Alert, Exchange, Pair, Ringtone, SelectOption, Target } from '@shared/models';
import { Set } from 'immutable';
import moment from 'moment';
import { filter } from 'rxjs/operators';
import * as uuid from 'uuid';

@Component({
  selector: 'app-alert-creation-form',
  templateUrl: './alert-creation-form.component.html',
  styleUrls: ['./alert-creation-form.component.scss']
})
export class AlertCreationFormComponent implements OnInit, OnChanges {

  @ViewChild('stepper') stepper: MatStepper;

  @Input() exchanges: Set<Exchange>;
  @Input() currentPrice?: number;
  @Input() ringtones: Set<Ringtone>;

  exchangeFormGroup: FormGroup;
  pairFormGroup: FormGroup;
  targetFormGroup: FormGroup;
  persistenceFormGroup: FormGroup;
  ringtoneFormGroup: FormGroup;
  targetPositions: Set<SelectOption<Target.Position>>;
  persistenceOptions: Set<SelectOption<boolean>>;


  constructor(private store: Store<AppStore.State>,
              private formBuilder: FormBuilder,
              private dialog: MatDialog) {
    this.targetPositions = Set(Object.keys(Target.Position).map(p => new SelectOption(Target.Position[p], p)));
    this.persistenceOptions = Set.of(new SelectOption(true, 'Every time'), new SelectOption(false, 'Only once'));
  }

  ngOnInit() {
    this.store.dispatch(new AlertCreationFormStore.Action.LoadExchanges());

    this.exchangeFormGroup = this.formBuilder.group({
      exchange: [null, Validators.required]
    });

    this.pairFormGroup = this.formBuilder.group({
      baseCurrency: [null, Validators.required],
      quoteCurrency: [{value: null, disabled: true}, Validators.required]
    });

    this.pairFormGroup.controls['baseCurrency'].valueChanges.subscribe(() => {
      this.pairFormGroup.controls['quoteCurrency'].reset(null);
      this.pairFormGroup.controls['quoteCurrency'].enable();
    });

    this.pairFormGroup.controls['quoteCurrency'].valueChanges.pipe(filter(Boolean)).subscribe(() => {
      const payload = {
        pair: Pair.newBuilder()
          .withBaseCurrency(this.pairFormGroup.controls['baseCurrency'].value)
          .withQuoteCurrency(this.pairFormGroup.controls['quoteCurrency'].value)
          .build(),
        exchange: this.exchangeFormGroup.controls['exchange'].value
      };

      this.store.dispatch(new AlertCreationFormStore.Action.UpdateCurrentPrice(payload));
    });

    this.targetFormGroup = this.formBuilder.group({
      position: [null,  Validators.required],
      price: [null, [Validators.required, Validators.min(0)]]
    });

    this.persistenceFormGroup = this.formBuilder.group({
      persistent: [null, Validators.required]
    });

    this.ringtoneFormGroup = this.formBuilder.group({
      ringtone: [null, Validators.required]
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.currentPrice && this.currentPrice) {
      this.targetFormGroup.controls['price'].setValue(this.currentPrice);
    }
  }

  openRingtoneCreationDialog() {
    this.dialog.open(RingtoneCreationDialogComponent).afterClosed()
      .pipe(filter(Boolean))
      .subscribe(ringtone => this.ringtoneFormGroup.controls['ringtone'].setValue(ringtone));
  }

  baseCurrencies(): Set<string> {
    const exchange = this.exchangeFormGroup.controls['exchange'].value;

    if (!exchange) {
      return Set();
    }

    return exchange.pairs
      .map(p => p.baseCurrency)
      .sortBy(c => c);
  }

  quoteCurrencies(): Set<string> {
    const exchange = this.exchangeFormGroup.controls['exchange'].value;
    const baseCurrency = this.pairFormGroup.controls['baseCurrency'].value;

    if (!exchange || !this.baseCurrencies) {
      return Set();
    }

    return exchange.pairs
      .filter(p => p.baseCurrency === baseCurrency)
      .map(p => p.quoteCurrency)
      .sortBy(c => c);
  }

  create() {
    const pair = Pair.newBuilder()
      .withBaseCurrency(this.pairFormGroup.controls['baseCurrency'].value)
      .withQuoteCurrency(this.pairFormGroup.controls['quoteCurrency'].value)
      .build();

    const target = Target.newBuilder()
      .withPrice(this.targetFormGroup.controls['price'].value)
      .withPosition(this.targetFormGroup.controls['position'].value)
      .build();

    const persistentOption = this.persistenceFormGroup.controls['persistent'].value;

    const alert = Alert.newBuilder()
      .withId(uuid.v4())
      .withExchangeName(this.exchangeFormGroup.controls['exchange'].value.name)
      .withPair(pair)
      .withTarget(target)
      .withRingtoneId(this.ringtoneFormGroup.controls['ringtone'].value.id)
      .withCreationDate(moment().utc())
      .withPersistent(persistentOption.value)
      .withEnabled(true)
      .withReady(false)
      .build();

    this.store.dispatch(new AlertsStore.Action.Add({alert: alert}));

    this.reset();
  }

  private reset() {
    // form validators reset issue: https://github.com/angular/material2/issues/4190

    this.stepper.reset();
    Object.keys(this.exchangeFormGroup.controls).forEach(k => this.exchangeFormGroup.controls[k].setErrors(null));
    Object.keys(this.pairFormGroup.controls).forEach(k => this.pairFormGroup.controls[k].setErrors(null));
    Object.keys(this.targetFormGroup.controls).forEach(k => this.targetFormGroup.controls[k].setErrors(null));
    Object.keys(this.persistenceFormGroup.controls).forEach(k => this.persistenceFormGroup.controls[k].setErrors(null));
    Object.keys(this.ringtoneFormGroup.controls).forEach(k => this.ringtoneFormGroup.controls[k].setErrors(null));
  }
}
