import { Injectable } from '@angular/core';
import { AlertPriceWatchService, AlertWatchService, SnackBarService } from '@core/services';
import { StorageService } from '@core/services/storage/storage.service';
import { AppStore } from '@core/store';
import { Actions as StoreActions, Effect, ofType } from '@ngrx/effects';
import { Action as StoreAction, Store } from '@ngrx/store';
import { Alert, StoreFailureAction, Target, WatchProcess, WatchResult } from '@shared/models';
import { LivePrice } from '@shared/models/trading/live-price.model';
import { OrderedSet, Set } from 'immutable';
import * as moment from 'moment';
import { Moment } from 'moment';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, filter, first, map, switchMap, tap } from 'rxjs/operators';

export namespace AlertsStore {

  export interface State {
    entities: OrderedSet<Alert>;
    watchProcesses: Set<WatchProcess>;
    livePrices: Set<LivePrice>;
  }

  export const initialState: State = {
    entities: OrderedSet(),
    watchProcesses: Set(),
    livePrices: Set()
  };

  export type Actions =
    | Action.Add

    | Action.Load
    | Action.LoadSuccess
    | Action.LoadFailure

    | Action.Update

    | Action.Delete

    | Action.Enable
    | Action.Disable

    | Action.Watch
    | Action.WatchSuccess
    | Action.WatchFailure

    | Action.Unwatch
    | Action.UnwatchSuccess
    | Action.UnwatchFailure

    | Action.WatchPrice
    | Action.WatchPriceSuccess
    | Action.WatchPriceFailure

    | Action.UnwatchPrice
    | Action.UnwatchPriceSuccess
    | Action.UnwatchPriceFailure

    | Action.UpdateLivePrice

    | Action.Trigger

    | Action.CheckErrors
    | Action.ErrorsCheckSuccess
    | Action.ErrorsCheckFailure;

  export namespace Action {

    export enum Type {
      ADD = '[Alerts] add',

      LOAD = '[Alerts] load',
      LOAD_SUCCESS = '[Alerts] load success',
      LOAD_FAILURE = '[Alerts] load failure',

      UPDATE = '[Alerts] update',

      DELETE = '[Alerts] delete',

      ENABLE = '[Alerts] enable',
      DISABLE = '[Alerts] disable',

      WATCH = '[Alerts] watch',
      WATCH_SUCCESS = '[Alerts] watch success',
      WATCH_FAILURE = '[Alerts] watch failure',

      UNWATCH = '[Alerts] unwatch',
      UNWATCH_SUCCESS = '[Alerts] unwatch success',
      UNWATCH_FAILURE = '[Alerts] unwatch failure',

      WATCH_PRICE = '[Alerts] watch price',
      WATCH_PRICE_SUCCESS = '[Alerts] watch price success',
      WATCH_PRICE_FAILURE = '[Alerts] watch price failure',

      UNWATCH_PRICE = '[Alerts] unwatch price',
      UNWATCH_PRICE_SUCCESS = '[Alerts] unwatch price success',
      UNWATCH_PRICE_FAILURE = '[Alerts] unwatch price failure',

      UPDATE_LIVE_PRICE = '[Alerts] update live price',

      TRIGGER = '[Alerts] trigger',

      CHECK_ERRORS = '[Alerts] check errors',
      ERRORS_CHECK_SUCCESS = '[Alerts] errors check success',
      ERRORS_CHECK_FAILURE = '[Alerts] errors check failure'
    }

    export class Add implements StoreAction {
      readonly type = Type.ADD;

      constructor(public payload: { alert: Alert }) {}
    }

    export class Load implements StoreAction {
      readonly type = Type.LOAD;
    }

    export class LoadSuccess implements StoreAction {
      readonly type = Type.LOAD_SUCCESS;

      constructor(public payload: { alerts: OrderedSet<Alert> }) {}
    }

    export class LoadFailure extends StoreFailureAction {
      readonly type = Type.LOAD_FAILURE;
    }

    export class Update implements StoreAction {
      readonly type = Type.UPDATE;

      constructor(public payload: { alert: Alert }) {}
    }

    export class Delete implements StoreAction {
      readonly type = Type.DELETE;

      constructor(public payload: { alert: Alert }) {}
    }

    export class Enable implements StoreAction {
      readonly type = Type.ENABLE;

      constructor(public payload: { alertId: string }) {}
    }

    export class Disable implements StoreAction {
      readonly type = Type.DISABLE;

      constructor(public payload: { alertId: string }) {}
    }

    export class Watch implements StoreAction {
      readonly type = Type.WATCH;

      constructor(public payload: { alertId: string }) {}
    }

    export class WatchSuccess implements StoreAction {
      readonly type = Type.WATCH_SUCCESS;

      constructor(public payload: { watchProcess: WatchProcess }) {}
    }

    export class WatchFailure extends StoreFailureAction {
      readonly type = Type.WATCH_FAILURE;
    }

    export class Unwatch implements StoreAction {
      readonly type = Type.UNWATCH;

      constructor(public payload: { alertId: string }) {}
    }

    export class UnwatchSuccess implements StoreAction {
      readonly type = Type.UNWATCH_SUCCESS;

      constructor(public payload: { watchProcess: WatchProcess }) {}
    }

    export class UnwatchFailure extends StoreFailureAction {
      readonly type = Type.UNWATCH_FAILURE;
    }

    export class WatchPrice implements StoreAction {
      readonly type = Type.WATCH_PRICE;

      constructor(public payload: { source: LivePrice.Source }) {}
    }

    export class WatchPriceSuccess implements StoreAction {
      readonly type = Type.WATCH_PRICE_SUCCESS;

      constructor(public payload: { livePrice: LivePrice }) {}
    }

    export class WatchPriceFailure extends StoreFailureAction {
      readonly type = Type.WATCH_PRICE_FAILURE;
    }

    export class UnwatchPrice implements StoreAction {
      readonly type = Type.UNWATCH_PRICE;

      constructor(public payload: { source: LivePrice.Source }) {}
    }

    export class UnwatchPriceSuccess implements StoreAction {
      readonly type = Type.UNWATCH_PRICE_SUCCESS;

      constructor(public payload: { livePrice: LivePrice }) {}
    }

    export class UnwatchPriceFailure extends StoreFailureAction {
      readonly type = Type.UNWATCH_PRICE_FAILURE;
    }

    export class UpdateLivePrice implements StoreAction {
      readonly type = Type.UPDATE_LIVE_PRICE;

      constructor(public payload: { source: LivePrice.Source, price?: number }) {}
    }

    export class Trigger implements StoreAction {
      readonly type = Type.TRIGGER;

      constructor(public payload: { alertId: string, target: Target, triggeredAt: Moment }) {}
    }

    export class CheckErrors implements StoreAction {
      readonly type = Type.CHECK_ERRORS;

      constructor(public payload: { alerts: OrderedSet<Alert> }) {}
    }

    export class ErrorsCheckSuccess implements StoreAction {
      readonly type = Type.ERRORS_CHECK_SUCCESS;

      constructor(public payload: { alerts: OrderedSet<Alert> }) {}
    }

    export class ErrorsCheckFailure extends StoreFailureAction {
      readonly type = Type.ERRORS_CHECK_FAILURE;
    }
  }

  export function reducer(state = initialState, action: Actions): State {
    switch (action.type) {
      case Action.Type.ADD: {
        return {
          ...state,
          entities: OrderedSet.of(action.payload.alert).merge(state.entities)
        };
      }

      case Action.Type.LOAD_SUCCESS: {
        return {
          ...state,
          entities: action.payload.alerts
        };
      }

      case Action.Type.UPDATE: {
        return {
          ...state,
          entities: state.entities.add(action.payload.alert)
        };
      }

      case Action.Type.DELETE: {
        return {
          ...state,
          entities: state.entities.remove(action.payload.alert)
        };
      }

      case Action.Type.ENABLE: {
        return {
          ...state,
          entities: state.entities.map(alert => Alert.copy(alert)
            .withEnabled(alert.id === action.payload.alertId ? true : alert.enabled)
            .build())
        };
      }

      case Action.Type.DISABLE: {
        return {
          ...state,
          entities: state.entities.map(alert => Alert.copy(alert)
              .withEnabled(alert.id === action.payload.alertId ? false : alert.enabled)
              .build())
        };
      }

      case Action.Type.WATCH_SUCCESS: {
        return {
          ...state,
          watchProcesses: state.watchProcesses.add(action.payload.watchProcess)
        };
      }

      case Action.Type.UNWATCH_SUCCESS: {
        return {
          ...state,
          watchProcesses: state.watchProcesses.remove(action.payload.watchProcess)
        };
      }

      case Action.Type.WATCH_PRICE_SUCCESS: {
        return {
          ...state,
          livePrices: state.livePrices.add(action.payload.livePrice)
        };
      }

      case Action.Type.UNWATCH_PRICE_SUCCESS: {
        return {
          ...state,
          livePrices: state.livePrices.remove(action.payload.livePrice)
        };
      }

      case Action.Type.UPDATE_LIVE_PRICE: {
        return {
          ...state,
          livePrices: state.livePrices.map(livePrice => {
            if (!livePrice.source.equals(action.payload.source))
              return livePrice;

            return LivePrice.copy(livePrice).withPrice(action.payload.price).build();
          })
        };
      }

      case Action.Type.TRIGGER: {
        return {
          ...state,
          entities: state.entities.map(alert => Alert.copy(alert)
            .withTriggeredAt(alert.id === action.payload.alertId ? alert.triggeredAt.add(action.payload.triggeredAt) : alert.triggeredAt)
            .withReady(alert.id === action.payload.alertId ? false : alert.ready)
            .withEnabled(alert.id === action.payload.alertId && !alert.persistent ? false : alert.enabled)
            .build())
        };
      }

      case Action.Type.ERRORS_CHECK_SUCCESS: {
        return {
          ...state,
          entities: state.entities.merge(action.payload.alerts)
        };
      }

      default:
        return state;
    }
  }

}

@Injectable()
export class AlertsStoreEffects {

  constructor(private actions$: StoreActions,
              private store: Store<AppStore.State>,
              private snackBarService: SnackBarService,
              private alertWatchService: AlertWatchService,
              private alertPriceWatchService: AlertPriceWatchService) {}

  @Effect({dispatch: false})
  add$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.ADD),
    map((action: AlertsStore.Action.Add) => action.payload.alert),
    tap(alert => StorageService.storeAlert(alert)),
    map(() => this.snackBarService.show('Alert successfully created.')));

  @Effect()
  load$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.LOAD),
    map(() => StorageService.storedAlerts()),
    map(alerts => new AlertsStore.Action.LoadSuccess({alerts: alerts})),
    catchError(err => of(new AlertsStore.Action.LoadFailure(err))));

  @Effect({dispatch: false})
  onUpdate$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.UPDATE),
    map((action: AlertsStore.Action.Update) => action.payload.alert),
    tap(alert => StorageService.updateAlert(alert)));

  @Effect({dispatch: false})
  delete$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.DELETE),
    map((action: AlertsStore.Action.Delete) => action.payload.alert),
    tap(alert => StorageService.removeAlert(alert)),
    map(() => this.snackBarService.show('Alert successfully deleted.')));

  @Effect()
  watch$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.WATCH),
    map((action: AlertsStore.Action.Watch) => action.payload.alertId),
    map(alertId => this.alertWatchService.createWatchProcess(alertId, AlertsStoreEffects.onWatch)),
    map(watchProcess => new AlertsStore.Action.WatchSuccess({watchProcess: watchProcess})),
    catchError(err => of(new AlertsStore.Action.WatchFailure(err))));

  @Effect()
  watchOnAdd$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.ADD),
    map((action: AlertsStore.Action.Add) => action.payload.alert),
    map(alert => new AlertsStore.Action.Watch({alertId: alert.id})));

  @Effect()
  watchOnEnable$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.ENABLE),
    map((action: AlertsStore.Action.Enable) => action.payload.alertId),
    map(alertId => new AlertsStore.Action.Watch({alertId: alertId})));

  @Effect()
  unwatch$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.UNWATCH),
    map((action: AlertsStore.Action.Unwatch) => action.payload.alertId),
    switchMap(alertId => this.getWatchProcessForAlert(alertId)),
    filter(Boolean),
    tap(watchProcess => watchProcess.subscription.unsubscribe()),
    map(watchProcess => new AlertsStore.Action.UnwatchSuccess({watchProcess: watchProcess})),
    catchError(err => of(new AlertsStore.Action.UnwatchFailure(err))));

  @Effect()
  unwatchOnDisable$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.DISABLE),
    map((action: AlertsStore.Action.Disable) => action.payload.alertId),
    map(alertId => new AlertsStore.Action.Unwatch({alertId: alertId})));

  @Effect()
  watchPrice$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.WATCH_PRICE),
    map((action: AlertsStore.Action.WatchPrice) => action.payload.source),
    map(source => this.alertPriceWatchService.createWatchProcess(source, AlertsStoreEffects.onPriceWatch)),
    map(livePrice => new AlertsStore.Action.WatchPriceSuccess({livePrice: livePrice})),
    catchError(err => of(new AlertsStore.Action.WatchPriceFailure(err))));

  @Effect()
  watchPriceOnAdd$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.ADD),
    map((action: AlertsStore.Action.Add) => action.payload.alert),
    map(alert => new LivePrice.Source(alert.pair, alert.exchangeName)),
    switchMap(source => this.store.select(state => state.alert.alerts.livePrices).pipe(
      filter(livePrices => livePrices.filter(l => l.source.equals(source)).isEmpty()),
      map(() => new AlertsStore.Action.WatchPrice({source: source})))));

  @Effect({dispatch: false})
  watchPriceOnLoad$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.LOAD_SUCCESS),
    switchMap(() => this.sourcesToWatch().pipe(
      tap(sources => sources.forEach(s => this.store.dispatch(new AlertsStore.Action.WatchPrice({source: s})))))));

  @Effect()
  unwatchPrice$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.UNWATCH_PRICE),
    map((action: AlertsStore.Action.UnwatchPrice) => action.payload.source),
    switchMap(source => this.getLivePrice(source)),
    filter(Boolean),
    tap((livePrice: LivePrice) => livePrice.subscription.unsubscribe()),
    map(livePrice => new AlertsStore.Action.UnwatchPriceSuccess({livePrice: livePrice})),
    catchError(err => of(new AlertsStore.Action.UnwatchPriceFailure(err))));

  @Effect()
  unwatchPriceOnDelete$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.DELETE),
    map((action: AlertsStore.Action.Delete) => action.payload.alert),
    switchMap(alert => this.store.select(state => state.alert.alerts.entities).pipe(
      filter(alerts => alerts.remove(alert).filter(a => a.pair.equals(alert.pair) && a.exchangeName === alert.exchangeName).isEmpty()),
      map(() => new AlertsStore.Action.UnwatchPrice({source: new LivePrice.Source(alert.pair, alert.exchangeName)})))));

  @Effect()
  checkErrors$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.CHECK_ERRORS),
    map((action: AlertsStore.Action.CheckErrors) => action.payload.alerts),
    switchMap(alerts => this.checkAlertsErrors(alerts)),
    map(alerts => new AlertsStore.Action.ErrorsCheckSuccess({alerts: OrderedSet(alerts)})),
    catchError(err => of(new AlertsStore.Action.ErrorsCheckFailure(err))));

  @Effect()
  checkErrorsOnLoadSuccess$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.LOAD_SUCCESS),
    map((action: AlertsStore.Action.LoadSuccess) => action.payload.alerts),
    map(alerts => new AlertsStore.Action.CheckErrors({alerts: alerts})));

  @Effect()
  checkErrorsOnUpdate$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.UPDATE),
    map((action: AlertsStore.Action.Update) => action.payload.alert),
    map(alert => new AlertsStore.Action.CheckErrors({alerts: OrderedSet.of(alert)})));

  @Effect({dispatch: false})
  ToggleAlertsAfterErrorCheck$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.ERRORS_CHECK_SUCCESS),
    map((action: AlertsStore.Action.ErrorsCheckSuccess) => action.payload.alerts),
    tap(alerts => alerts.forEach(alert => {
      if (alert.errors.isEmpty() && !alert.enabled) {
        this.store.dispatch(new AlertsStore.Action.Enable({alertId: alert.id}));
      } else if (!alert.errors.isEmpty() && alert.enabled) {
        this.store.dispatch(new AlertsStore.Action.Disable({alertId: alert.id}));
      }
    })));

  private checkAlertsErrors(alerts: OrderedSet<Alert>): Observable<Alert[]> {
    return forkJoin(alerts.map(alert => this.checkAlertRingtoneError(alert)).toArray());
  }

  private checkAlertRingtoneError(alert: Alert): Observable<Alert> {
    return this.store.select(state => state.alert.ringtones.entities).pipe(
      filter(Boolean),
      map(ringtones => ringtones.filter(r => r.id === alert.ringtoneId)),
      map(ringtones => !ringtones.isEmpty()),
      map(ringtonePresent => Alert.copy(alert)
        .withErrors(ringtonePresent ? alert.errors.remove(Alert.Error.MISSING_RINGTONE) : alert.errors.add(Alert.Error.MISSING_RINGTONE))
        .build()),
      first());
  }

  private getWatchProcessForAlert(alertId: string): Observable<WatchProcess | undefined> {
    return this.store.select(state =>  state.alert.alerts.watchProcesses).pipe(
      map(processes => processes.find(p => p.alertId === alertId)));
  }

  private getLivePrice(source: LivePrice.Source): Observable<LivePrice | undefined> {
    return this.store.select(state => state.alert.alerts.livePrices).pipe(
      map(livePrices => livePrices.find(l => l.source.equals(source))));
  }

  private sourcesToWatch(): Observable<Set<LivePrice.Source>> {
    return this.store.select(state => state.alert.alerts.entities).pipe(
      map(alerts => alerts.map(a => new LivePrice.Source(a.pair, a.exchangeName))),
      map(sources => sources.toSet()));
  }

  private static onWatch(store: Store<AppStore.State>, result: WatchResult) {
    if (!result.triggered && !result.alert.ready) {
      const updatedAlert = Alert.copy(result.alert)
        .withReady(true)
        .build();

      store.dispatch(new AlertsStore.Action.Update({alert: updatedAlert}));
    } else if (result.triggered && result.alert.ready) {
      const payload = {
        alertId: result.alert.id,
        target: result.alert.target,
        triggeredAt: moment.utc()
      };

      store.dispatch(new AlertsStore.Action.Trigger(payload));
    }
  }

  private static onPriceWatch(store: Store<AppStore.State>, source: LivePrice.Source, price: number) {
    store.dispatch(new AlertsStore.Action.UpdateLivePrice({source: source, price: price}));
  }

}
