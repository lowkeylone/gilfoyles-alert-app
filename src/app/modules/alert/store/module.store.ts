import { AlertCreationFormStore } from '@modules/alert/store/alert-creation-form.store';
import { AlertsStore } from '@modules/alert/store/alerts.store';
import { RingtonesStore } from '@modules/alert/store/ringtones.store';

export namespace AlertModuleStore {

  export interface State {
    creationForm: AlertCreationFormStore.State;
    ringtones: RingtonesStore.State;
    alerts: AlertsStore.State;
  }

  export const reducers = {
    creationForm: AlertCreationFormStore.reducer,
    ringtones: RingtonesStore.reducer,
    alerts: AlertsStore.reducer
  };
}
