import { Injectable } from '@angular/core';
import { SnackBarService } from '@core/services';
import { Actions as StoreActions, Effect, ofType } from '@ngrx/effects';
import { Action as StoreAction } from '@ngrx/store';
import { Ringtone } from '@shared/models';
import { Set } from 'immutable';
import { map } from 'rxjs/operators';

export namespace RingtonesStore {

  export interface State {
    entities: Set<Ringtone>;
  }

  export const initialState: State = {
    entities: Set()
  };

  export type Actions =
    | Action.Add;

  export namespace Action {

    export enum Type {
      ADD = '[Ringtones] add'
    }

    export class Add implements StoreAction {
      readonly type = Type.ADD;

      constructor(public payload: { ringtone: Ringtone }) {}
    }

  }

  export function reducer(state = initialState, action: Actions): State {
    switch (action.type) {
      case Action.Type.ADD: {
        return {
          ...state,
          entities: state.entities.add(action.payload.ringtone)
        };
      }

      default:
        return state;
    }
  }

}

@Injectable()
export class RingtonesStoreEffects {

  constructor(private actions$: StoreActions,
              private snackBarService: SnackBarService) {}

  @Effect({dispatch: false})
  add$ = this.actions$.pipe(
    ofType(RingtonesStore.Action.Type.ADD),
    map((action: RingtonesStore.Action.Add) => action.payload.ringtone),
    map(() => this.snackBarService.show('Ringtone successfully added.')));
}
