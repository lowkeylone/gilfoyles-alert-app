import { Injectable } from '@angular/core';
import { CryptoCompareService } from '@core/services';
import { Actions as StoreActions, Effect, ofType } from '@ngrx/effects';
import { Action as StoreAction } from '@ngrx/store';
import { Exchange, Pair, StoreFailureAction } from '@shared/models';
import { OrderedSet } from 'immutable';
import { of } from 'rxjs/internal/observable/of';
import { catchError, map, switchMap } from 'rxjs/operators';

export namespace AlertCreationFormStore {

  export interface State {
    exchanges: OrderedSet<Exchange>;
    currentPrice?: number;
  }

  export const initialState: State = {
    exchanges: OrderedSet(),
    currentPrice: undefined
  };

  export type Actions =
    | Action.LoadExchanges
    | Action.ExchangesLoadSuccess
    | Action.ExchangesLoadFailure

    | Action.UpdateCurrentPrice
    | Action.CurrentPriceUpdateSuccess
    | Action.CurrentPriceUpdateFailure;

  export namespace Action {

    export enum Type {
      LOAD_EXCHANGES = '[AlertCreationForm] load exchanges',
      EXCHANGES_LOAD_SUCCESS = '[AlertCreationForm] exchanges load success',
      EXCHANGES_LOAD_FAILURE = '[AlertCreationForm] exchanges load failure',

      UPDATE_CURRENT_PRICE = '[AlertCreationForm] update current price',
      CURRENT_PRICE_UPDATE_SUCCESS = '[AlertCreationForm] current price update success',
      CURRENT_PRICE_UPDATE_FAILURE = '[AlertCreationForm] current price update failure'
    }

    export class LoadExchanges implements StoreAction {
      readonly type = Type.LOAD_EXCHANGES;
    }

    export class ExchangesLoadSuccess implements StoreAction {
      readonly type = Type.EXCHANGES_LOAD_SUCCESS;

      constructor(public payload: { exchanges: OrderedSet<Exchange> }) {}
    }

    export class ExchangesLoadFailure extends StoreFailureAction {
      readonly type = Type.EXCHANGES_LOAD_FAILURE;
    }

    export class UpdateCurrentPrice implements StoreAction {
      readonly type = Type.UPDATE_CURRENT_PRICE;

      constructor(public payload: { pair: Pair, exchange: Exchange }) {}
    }

    export class CurrentPriceUpdateSuccess implements StoreAction {
      readonly type = Type.CURRENT_PRICE_UPDATE_SUCCESS;

      constructor(public payload: { currentPrice: number | undefined }) {}
    }

    export class CurrentPriceUpdateFailure extends StoreFailureAction {
      readonly type = Type.CURRENT_PRICE_UPDATE_FAILURE;
    }

  }

  export function reducer(state = initialState, action: Actions): State {
    switch (action.type) {
      case Action.Type.EXCHANGES_LOAD_SUCCESS: {
        return {
          ...state,
          exchanges: action.payload.exchanges
        };
      }

      case Action.Type.CURRENT_PRICE_UPDATE_SUCCESS: {
        return {
          ...state,
          currentPrice: action.payload.currentPrice
        };
      }

      default:
        return state;
    }
  }

}

@Injectable()
export class AlertCreationFormStoreEffects {

  constructor(private actions$: StoreActions, private cryptoCompareService: CryptoCompareService) {}

  @Effect()
  loadExchanges$ = this.actions$.pipe(
    ofType(AlertCreationFormStore.Action.Type.LOAD_EXCHANGES),
    switchMap(() => this.cryptoCompareService.allExchanges().pipe(
      map(response => response.data),
      map(data => data.sortBy(e => e.name).toOrderedSet()),
      map(exchanges => new AlertCreationFormStore.Action.ExchangesLoadSuccess({exchanges: exchanges})),
      catchError(err => of(new AlertCreationFormStore.Action.ExchangesLoadFailure(err))))));

  @Effect()
  updateCurrentPrice$ = this.actions$.pipe(
    ofType(AlertCreationFormStore.Action.Type.UPDATE_CURRENT_PRICE),
    map((action: AlertCreationFormStore.Action.UpdateCurrentPrice) => action.payload),
    switchMap(payload => this.cryptoCompareService.singleSymbolPrice(payload.pair, payload.exchange.name).pipe(
      map(response => response.get(payload.pair.quoteCurrency, undefined)),
      map(price => new AlertCreationFormStore.Action.CurrentPriceUpdateSuccess({currentPrice: price})),
      catchError(err => of(new AlertCreationFormStore.Action.CurrentPriceUpdateFailure(err))))));
}
