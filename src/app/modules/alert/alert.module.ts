import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { AlertCreationFormComponent } from '@modules/alert/components';
import { AlertComponent } from '@modules/alert/containers';
import { AlertModuleStore } from '@modules/alert/store';
import { StoreModule } from '@ngrx/store';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([{path: '', component: AlertComponent}]),
    StoreModule.forFeature('alert', AlertModuleStore.reducers)
  ],
  declarations: [
    AlertComponent,
    AlertCreationFormComponent
  ]
})
export class AlertModule {}
