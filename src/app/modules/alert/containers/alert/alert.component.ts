import { Component, OnInit } from '@angular/core';
import { AppStore } from '@core/store';
import { Store } from '@ngrx/store';
import { Exchange, Ringtone } from '@shared/models';
import { OrderedSet, Set } from 'immutable';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  exchanges$: Observable<OrderedSet<Exchange>>;
  currentPrice$: Observable<number | undefined>;
  ringtones$: Observable<Set<Ringtone>>;

  constructor(private store: Store<AppStore.State>) {}

  ngOnInit() {
    this.exchanges$ = this.store.select(state => state.alert.creationForm.exchanges);
    this.currentPrice$ = this.store.select(state => state.alert.creationForm.currentPrice);
    this.ringtones$ = this.store.select(state => state.alert.ringtones.entities);
  }
}
