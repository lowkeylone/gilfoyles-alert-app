import { Component, Input } from '@angular/core';
import { Alert, LivePrice, Ringtone } from '@shared/models';
import { OrderedSet, Set } from 'immutable';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent {

  @Input() opened: boolean;
  @Input() alerts: OrderedSet<Alert>;
  @Input() ringtones: Set<Ringtone>;
  @Input() livePrices: Set<LivePrice>;
}
