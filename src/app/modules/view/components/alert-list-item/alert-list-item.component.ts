import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppStore } from '@core/store';
import { AlertsStore } from '@modules/alert/store';
import { Store } from '@ngrx/store';
import { AlertEditionDialogComponent, AlertEditionDialogData } from '@shared/components';
import { Alert, LivePrice, Ringtone } from '@shared/models';
import { Set } from 'immutable';

@Component({
  selector: 'app-alert-list-item',
  templateUrl: './alert-list-item.component.html',
  styleUrls: ['./alert-list-item.component.scss'],
  host: {'class': 'alert-list-item'}
})
export class AlertListItemComponent implements OnChanges {

  @Input() alert: Alert;
  @Input() ringtones: Set<Ringtone>;
  @Input() livePrices: Set<LivePrice>;

  livePrice?: LivePrice;

  constructor(private store: Store<AppStore.State>, private dialog: MatDialog) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.livePrices) {
      this.livePrice = this.livePrices.find(l => l.source.pair.equals(this.alert.pair) && l.source.exchangeName === this.alert.exchangeName);
    }
  }

  edit() {
    const data: AlertEditionDialogData = {
      alert: this.alert,
      ringtones: this.ringtones
    };

    this.dialog.open(AlertEditionDialogComponent, {data: data});
  }

  toggle() {
    if (this.alert.enabled) {
      this.store.dispatch(new AlertsStore.Action.Disable({ alertId: this.alert.id }));
    } else {
      this.store.dispatch(new AlertsStore.Action.Enable({ alertId: this.alert.id }));
    }
  }
}
