import { Component, Input } from '@angular/core';
import { AppStore } from '@core/store/app.store';
import { SideNavStore } from '@modules/view/store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {

  @Input() apiKey$: Observable<string | undefined>;

  constructor(private store: Store<AppStore.State>) {}

  toggleSideNav() {
    this.store.dispatch(new SideNavStore.Action.Toggle());
  }
}
