import { Component, Input } from '@angular/core';
import { Alert, LivePrice, Ringtone } from '@shared/models';
import { OrderedSet, Set } from 'immutable';

@Component({
  selector: 'app-alert-list',
  templateUrl: './alert-list.component.html',
  styleUrls: ['./alert-list.component.scss']
})
export class AlertListComponent {

  @Input() alerts: OrderedSet<Alert>;
  @Input() ringtones: Set<Ringtone>;
  @Input() livePrices: Set<LivePrice>;
}
