import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AppStore, AuthStore } from '@core/store';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-crypto-api-key',
  templateUrl: './crypto-api-key.component.html',
  styleUrls: ['./crypto-api-key.component.scss']
})
export class CryptoApiKeyComponent implements OnInit, OnChanges {

  @Input() apiKey?: string;

  apiKeyField: FormControl;

  constructor(private store: Store<AppStore.State>) {}

  ngOnInit() {
    this.apiKeyField = new FormControl('', [Validators.required]);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.apiKey && this.apiKeyField) {
      this.apiKeyField.setValue(this.apiKey || '');
    }
  }

  updateApiKey() {
    if (this.apiKeyField.invalid) return;

    this.store.dispatch(new AuthStore.Action.UpdateApiKey({apiKey: this.apiKeyField.value}));
  }
}
