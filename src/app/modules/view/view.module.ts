import { NgModule } from '@angular/core';
import { ViewComponent } from '@app/modules/view/containers';
import { SharedModule } from '@app/shared/shared.module';
import { AlertListComponent, AlertListItemComponent, CryptoApiKeyComponent, SidenavComponent, ToolbarComponent } from '@modules/view/components';
import { ViewModuleStore } from '@modules/view/store';
import { StoreModule } from '@ngrx/store';

@NgModule({
  imports: [
    SharedModule,
    StoreModule.forFeature('view', ViewModuleStore.reducers)
  ],
  declarations: [
    ViewComponent,
    SidenavComponent,
    ToolbarComponent,
    CryptoApiKeyComponent,
    AlertListComponent,
    AlertListItemComponent
  ]
})
export class ViewModule {
}
