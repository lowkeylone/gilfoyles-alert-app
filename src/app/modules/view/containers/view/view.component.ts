import { Component, OnInit } from '@angular/core';
import { AppStore } from '@core/store/app.store';
import { AlertsStore } from '@modules/alert/store';
import { Store } from '@ngrx/store';
import { Alert, LivePrice, Ringtone } from '@shared/models';
import { OrderedSet, Set } from 'immutable';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  apiKey$: Observable<string | undefined>;
  sideNavOpened$: Observable<boolean>;
  alerts$: Observable<OrderedSet<Alert>>;
  ringtones$: Observable<Set<Ringtone>>;
  livePrices$: Observable<Set<LivePrice>>;

  constructor(private store: Store<AppStore.State>) {}

  ngOnInit() {
    this.apiKey$ = this.store.select(state => state.auth.apiKey);
    this.sideNavOpened$ = this.store.select(state => state.view.sideNav.open);
    this.alerts$ = this.store.select(state => state.alert.alerts.entities);
    this.ringtones$ = this.store.select(state => state.alert.ringtones.entities);
    this.livePrices$ = this.store.select(state => state.alert.alerts.livePrices);

    this.store.dispatch(new AlertsStore.Action.Load());
  }
}
