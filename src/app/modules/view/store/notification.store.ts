import { Injectable } from '@angular/core';
import { AppStore } from '@core/store';
import { AlertsStore } from '@modules/alert/store';
import { Actions as StoreActions, Effect, ofType } from '@ngrx/effects';
import { Action as StoreAction, Store } from '@ngrx/store';
import { TriggerNotification } from '@shared/models';
import { OrderedSet } from 'immutable';
import { map, switchMap, tap } from 'rxjs/operators';

export namespace NotificationStore {

  export interface State {
    triggerNotifications: OrderedSet<TriggerNotification>;
  }

  export const initialState: State = {
    triggerNotifications: OrderedSet()
  };

  export type Actions =
    | Action.AddTriggerNotification
    | Action.RemoveTriggerNotification;

  export namespace Action {

    export enum Type {
      ADD_TRIGGER_NOTIFICATION = '[Notification] add trigger notification',
      REMOVE_TRIGGER_NOTIFICATION = '[Notification] remove trigger notification'
    }

    export class AddTriggerNotification implements StoreAction {
      readonly type = Type.ADD_TRIGGER_NOTIFICATION;

      constructor(public payload: { notification: TriggerNotification }) {}
    }

    export class RemoveTriggerNotification implements StoreAction {
      readonly type = Type.REMOVE_TRIGGER_NOTIFICATION;

      constructor(public payload: { timestamp: number }) {}
    }

  }

  export function reducer(state = initialState, action: Actions): State {
    switch (action.type) {
      case Action.Type.ADD_TRIGGER_NOTIFICATION: {
        return {
          ...state,
          triggerNotifications: OrderedSet.of(action.payload.notification).merge(state.triggerNotifications)
        };
      }

      case Action.Type.REMOVE_TRIGGER_NOTIFICATION: {
        return {
          ...state,
          triggerNotifications: state.triggerNotifications.filter(n => n.timestamp !== action.payload.timestamp)
        };
      }

      default:
        return state;
    }
  }

}

@Injectable()
export class NotificationStoreEffects {

  constructor(private actions$: StoreActions, private store: Store<AppStore.State>) {}

  @Effect()
  addTriggerNotificationOnAlertTrigger$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.TRIGGER),
    map((action: AlertsStore.Action.Trigger) => NotificationStoreEffects.buildTriggerNotification(action)),
    map(notification => new NotificationStore.Action.AddTriggerNotification({notification: notification})));

  @Effect({dispatch: false})
  removeTriggerNotificationsOnAlertDelete$ = this.actions$.pipe(
    ofType(AlertsStore.Action.Type.DELETE),
    map((action: AlertsStore.Action.Delete) => action.payload.alert),
    switchMap(alert => this.store.select(state => state.view.notification.triggerNotifications).pipe(
      map(notifications => notifications.filter(n => n.alertId === alert.id)))),
    tap(notifications => notifications.forEach(n => {
      this.store.dispatch(new NotificationStore.Action.RemoveTriggerNotification({timestamp: n.timestamp}));
    })));

  private static buildTriggerNotification(action: AlertsStore.Action.Trigger): TriggerNotification {
    return TriggerNotification.newBuilder()
      .withTimestamp(action.payload.triggeredAt.unix())
      .withAlertId(action.payload.alertId)
      .withTarget(action.payload.target)
      .withRead(false)
      .build();
  }
}
