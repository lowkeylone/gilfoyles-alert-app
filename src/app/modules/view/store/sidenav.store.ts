import { Action as StoreAction } from '@ngrx/store';

export namespace SideNavStore {

  export interface State {
    open: boolean;
  }

  export const initialState: State = {
    open: true
  };

  export type Actions =
    | Action.Open
    | Action.Close
    | Action.Toggle;

  export namespace Action {

    export enum Type {
      OPEN = '[SideNav] open',
      CLOSE = '[SideNav] close',
      TOGGLE = '[SideNav] toggle'
    }

    export class Open implements StoreAction {
      readonly type = Type.OPEN;
    }

    export class Close implements StoreAction {
      readonly type = Type.CLOSE;
    }

    export class Toggle implements StoreAction {
      readonly type = Type.TOGGLE;
    }

  }

  export function reducer(state = initialState, action: Actions): State {
    switch (action.type) {
      case Action.Type.OPEN: {
        return {
          ...state,
          open: true
        };
      }

      case Action.Type.CLOSE: {
        return {
          ...state,
          open: false
        };
      }

      case Action.Type.TOGGLE: {
        return {
          ...state,
          open: !state.open
        };
      }

      default:
        return state;
    }
  }

}
