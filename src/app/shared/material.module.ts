import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatDividerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatSelectModule, MatSidenavModule, MatStepperModule, MatToolbarModule, MatTooltipModule } from '@angular/material';

@NgModule({
  imports: [
    FormsModule,
    MatDividerModule,
    MatInputModule,
    MatSidenavModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatFormFieldModule,
    MatSelectModule,
    MatStepperModule,
    MatTooltipModule,
    MatDialogModule,
    MatListModule
  ],
  exports: [
    FormsModule,
    MatDividerModule,
    MatInputModule,
    MatSidenavModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatFormFieldModule,
    MatSelectModule,
    MatStepperModule,
    MatTooltipModule,
    MatDialogModule,
    MatListModule
  ],
  providers: []
})

export class MaterialModule {}
