import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '@app/shared/material.module';
import { AlertEditionDialogComponent, RingtoneCreationDialogComponent } from '@shared/components';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
  declarations: [
    AlertEditionDialogComponent,
    RingtoneCreationDialogComponent
  ],
  exports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule,
    AlertEditionDialogComponent,
    RingtoneCreationDialogComponent,
  ],
  providers: [],
  entryComponents: [AlertEditionDialogComponent, RingtoneCreationDialogComponent]
})

export class SharedModule {}
