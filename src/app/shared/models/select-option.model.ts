export class SelectOption<T> {
  constructor(public value: T, public name: string) {}
}
