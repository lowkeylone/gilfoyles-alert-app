export interface Notification {

  readonly timestamp: number;
  readonly read: boolean;

}
