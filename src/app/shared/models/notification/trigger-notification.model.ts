import { Target } from '@shared/models/alert/target.model';
import { Notification } from '@shared/models/notification/notification.model';
import { hash, is, ValueObject } from 'immutable';

export class TriggerNotification implements Notification, ValueObject {

  readonly timestamp: number;
  readonly read: boolean;
  readonly alertId: string;
  readonly target: Target;

  constructor(builder: TriggerNotification.Builder) {
    this.timestamp = builder.timestamp;
    this.read = builder.read;
    this.alertId = builder.alertId;
    this.target = builder.target;
  }

  public static newBuilder(): TriggerNotification.Builder {
    return new TriggerNotification.Builder();
  }

  public static copy(triggerNotification: TriggerNotification): TriggerNotification.Builder {
    return TriggerNotification.newBuilder()
      .withTimestamp(triggerNotification.timestamp)
      .withRead(triggerNotification.read)
      .withAlertId(triggerNotification.alertId)
      .withTarget(triggerNotification.target);
  }

  equals(other: any): boolean {
    if (!(other instanceof TriggerNotification))
      return false;

    return is(this.timestamp, other.timestamp);
  }

  hashCode(): number {
    return hash(this.timestamp);
  }

}

export namespace TriggerNotification {

  export class Builder {

    timestamp: number;
    read: boolean;
    alertId: string;
    target: Target;

    withTimestamp(timestamp: number): Builder {
      this.timestamp = timestamp;
      return this;
    }

    withRead(read: boolean): Builder {
      this.read = read;
      return this;
    }

    withAlertId(alertId: string): Builder {
      this.alertId = alertId;
      return this;
    }

    withTarget(target: Target): Builder {
      this.target = target;
      return this;
    }

    build(): TriggerNotification {
      return new TriggerNotification(this);
    }

  }

}
