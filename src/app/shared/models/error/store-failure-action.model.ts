import { Action as StoreAction } from '@ngrx/store';

export class StoreFailureAction implements StoreAction {

  readonly type: string;

  constructor(public payload: Error) {}

}
