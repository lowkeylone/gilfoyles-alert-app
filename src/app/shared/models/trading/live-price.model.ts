import { Arrays } from '@shared/models/arrays.model';
import { Pair } from '@shared/models/trading/pair.model';
import { is, ValueObject } from 'immutable';
import { Subscription } from 'rxjs';

export class LivePrice implements ValueObject {

  readonly source: LivePrice.Source;
  readonly price?: number;
  readonly subscription: Subscription;

  constructor(builder: LivePrice.Builder) {
    this.source = builder.source;
    this.price = builder.price;
    this.subscription = builder.subscription;
  }

  public static newBuilder(): LivePrice.Builder {
    return new LivePrice.Builder();
  }

  public static copy(livePrice: LivePrice): LivePrice.Builder {
    return LivePrice.newBuilder()
      .withSource(livePrice.source)
      .withPrice(livePrice.price)
      .withSubscription(livePrice.subscription);
  }

  public equals(other: any): boolean {
    if (!(other instanceof LivePrice))
      return false;

    return is(this.source, other.source);
  }

  public hashCode(): number {
    return Arrays.hashCode(this.source);
  }

}

export namespace LivePrice {

  export class Builder {

    source: Source;
    price?: number;
    subscription: Subscription;

    withSource(source: Source): Builder {
      this.source = source;
      return this;
    }

    withPrice(price?: number): Builder {
      this.price = price;
      return this;
    }

    withSubscription(subscription: Subscription): Builder {
      this.subscription = subscription;
      return this;
    }

    build(): LivePrice {
      return new LivePrice(this);
    }

  }

  export class Source implements ValueObject {

    constructor(readonly pair: Pair, readonly exchangeName: string) {}

    public equals(other: any): boolean {
      if (!(other instanceof Source))
        return false;

      return is(this.pair, other.pair) && is(this.exchangeName, other.exchangeName);
    }

    public hashCode(): number {
      return Arrays.hashCode(this.pair, this.exchangeName);
    }

  }

}
