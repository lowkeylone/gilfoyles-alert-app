import { Arrays } from '@shared/models/arrays.model';
import { Pair } from '@shared/models/trading/pair.model';
import { is, Set, ValueObject } from 'immutable';

export class Exchange implements ValueObject {

  readonly name: string;
  readonly pairs: Set<Pair>;

  constructor(builder: Exchange.Builder) {
    this.name = builder.name;
    this.pairs = builder.pairs;
  }

  public static newBuilder(): Exchange.Builder {
    return new Exchange.Builder();
  }

  public static copy(exchange: Exchange): Exchange.Builder {
    return Exchange.newBuilder()
      .withName(exchange.name)
      .withPairs(exchange.pairs);
  }

  public equals(other: any): boolean {
    if (!(other instanceof Exchange))
      return false;

    return is(this.name, other.name) &&
      is(this.pairs, other.pairs);
  }

  public hashCode(): number {
    return Arrays.hashCode(this.name, this.pairs);
  }

}

export namespace Exchange {

  export class Builder {

    name: string;
    pairs: Set<Pair>;

    withName(name: string): Builder {
      this.name = name;
      return this;
    }

    withPairs(pairs: Set<Pair>): Builder {
      this.pairs = pairs;
      return this;
    }

    build(): Exchange {
      return new Exchange(this);
    }

  }

}
