import { Arrays } from '@shared/models/arrays.model';
import { is, ValueObject } from 'immutable';

export class Pair implements ValueObject {

  readonly baseCurrency: string;
  readonly quoteCurrency: string;

  constructor(builder: Pair.Builder) {
    this.baseCurrency = builder.baseCurrency;
    this.quoteCurrency = builder.quoteCurrency;
  }

  public static newBuilder(): Pair.Builder {
    return new Pair.Builder();
  }

  public static copy(pair: Pair): Pair.Builder {
    return Pair.newBuilder()
      .withBaseCurrency(pair.baseCurrency)
      .withQuoteCurrency(pair.quoteCurrency);
  }

  public static fromJson(json: any): Pair {
    return Pair.newBuilder()
      .withBaseCurrency(json.baseCurrency)
      .withQuoteCurrency(json.quoteCurrency)
      .build();
  }

  public equals(other: any): boolean {
    if (!(other instanceof Pair))
      return false;

    return is(this.baseCurrency, other.baseCurrency) &&
      is(this.quoteCurrency, other.quoteCurrency);
  }

  public hashCode(): number {
    return Arrays.hashCode(this.baseCurrency, this.quoteCurrency);
  }

  public toString(): string {
    return `${this.baseCurrency}/${this.quoteCurrency}`;
  }
}

export namespace Pair {

  export class Builder {

    baseCurrency: string;
    quoteCurrency: string;

    withBaseCurrency(baseCurrency: string): Builder {
      this.baseCurrency = baseCurrency;
      return this;
    }

    withQuoteCurrency(quoteCurrency: string): Builder {
      this.quoteCurrency = quoteCurrency;
      return this;
    }

    build(): Pair {
      return new Pair(this);
    }
  }
}
