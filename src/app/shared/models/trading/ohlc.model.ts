import { hash, is, ValueObject } from 'immutable';
import * as moment from 'moment';
import { Moment } from 'moment';

export class OHLC implements ValueObject {

  readonly date: Moment;
  readonly open: number;
  readonly high: number;
  readonly low: number;
  readonly close: number;

  constructor(builder: OHLC.Builder) {
    this.date = builder.date;
    this.open = builder.open;
    this.high = builder.high;
    this.low = builder.low;
    this.close = builder.close;
  }

  public static newBuilder(): OHLC.Builder {
    return new OHLC.Builder();
  }

  public static copy(ohlc: OHLC): OHLC.Builder {
    return OHLC.newBuilder()
      .withDate(ohlc.date)
      .withOpen(ohlc.open)
      .withHigh(ohlc.high)
      .withLow(ohlc.low)
      .withClose(ohlc.close);
  }

  public static fromJson(json: any): OHLC {
    return OHLC.newBuilder()
      .withDate(moment.unix(json.time))
      .withOpen(json.open)
      .withHigh(json.high)
      .withLow(json.low)
      .withClose(json.close)
      .build();
  }

  public equals(other: any): boolean {
    if (!(other instanceof OHLC))
      return false;

    return is(this.date, other.date);
  }

  public hashCode(): number {
    return hash(this.date);
  }

}

export namespace OHLC {

  export class Builder {

    date: Moment;
    open: number;
    high: number;
    low: number;
    close: number;

    withDate(date: Moment): Builder {
      this.date = date;
      return this;
    }

    withOpen(open: number): Builder {
      this.open = open;
      return this;
    }

    withHigh(high: number): Builder {
      this.high = high;
      return this;
    }

    withLow(low: number): Builder {
      this.low = low;
      return this;
    }

    withClose(close: number): Builder {
      this.close = close;
      return this;
    }

    build(): OHLC {
      return new OHLC(this);
    }

  }

}
