import { Target } from '@shared/models/alert/target.model';
import { Pair } from '@shared/models/trading/pair.model';
import { hash, is, OrderedSet, Set, ValueObject } from 'immutable';
import * as moment from 'moment';
import { Moment } from 'moment';

export class Alert implements ValueObject {

  readonly id: string;
  readonly exchangeName: string;
  readonly pair: Pair;
  readonly target: Target;
  readonly ringtoneId: string;
  readonly creationDate: Moment;
  readonly persistent: boolean;
  readonly errors: Set<Alert.Error>;
  readonly triggeredAt: OrderedSet<Moment>;
  readonly enabled: boolean;
  readonly ready: boolean;

  constructor(builder: Alert.Builder) {
    this.id = builder.id;
    this.exchangeName = builder.exchangeName;
    this.pair = builder.pair;
    this.target = builder.target;
    this.ringtoneId = builder.ringtoneId;
    this.creationDate = builder.creationDate;
    this.persistent = builder.persistent;
    this.errors = builder.errors;
    this.triggeredAt = builder.triggeredAt;
    this.enabled = builder.enabled;
    this.ready = builder.ready;
  }

  public static newBuilder(): Alert.Builder {
    return new Alert.Builder();
  }

  public static copy(alert: Alert): Alert.Builder {
    return Alert.newBuilder()
      .withId(alert.id)
      .withExchangeName(alert.exchangeName)
      .withPair(alert.pair)
      .withTarget(alert.target)
      .withRingtoneId(alert.ringtoneId)
      .withCreationDate(alert.creationDate)
      .withPersistent(alert.persistent)
      .withErrors(alert.errors)
      .withTriggeredAt(alert.triggeredAt)
      .withEnabled(alert.enabled)
      .withReady(alert.ready);
  }

  public static fromJson(json: any): Alert {
    return Alert.newBuilder()
      .withId(json.id)
      .withExchangeName(json.exchangeName)
      .withPair(Pair.fromJson(json.pair))
      .withTarget(Target.fromJson(json.target))
      .withRingtoneId(json.ringtoneId)
      .withCreationDate(moment.utc(json.creationDate))
      .withPersistent(json.persistent)
      .withErrors(Set(json.errors).map(e => Alert.Error[<string>e]))
      .withTriggeredAt(OrderedSet(json.triggeredAt).map(v => moment.utc(v)))
      .withEnabled(json.enabled)
      .withReady(json.ready)
      .build();
  }

  public equals(other: any): boolean {
    if (!(other instanceof Alert))
      return false;

    return is(this.id, other.id);
  }

  public hashCode(): number {
    return hash(this.id);
  }

}

export namespace Alert {

  export enum Error {
    MISSING_RINGTONE = 'MISSING_RINGTONE'
  }

  export class Builder {

    id: string;
    exchangeName: string;
    pair: Pair;
    target: Target;
    ringtoneId: string;
    creationDate: Moment;
    persistent: boolean;
    errors: Set<Error>;
    triggeredAt: OrderedSet<Moment>;
    enabled: boolean;
    ready: boolean;

    constructor() {
      this.errors = Set();
      this.triggeredAt = OrderedSet();
    }

    withId(id: string): Builder {
      this.id = id;
      return this;
    }

    withExchangeName(exchangeName: string): Builder {
      this.exchangeName = exchangeName;
      return this;
    }

    withPair(pair: Pair): Builder {
      this.pair = pair;
      return this;
    }

    withTarget(target: Target): Builder {
      this.target = target;
      return this;
    }

    withRingtoneId(ringtoneId: string): Builder {
      this.ringtoneId = ringtoneId;
      return this;
    }

    withCreationDate(creationDate: Moment): Builder {
      this.creationDate = creationDate;
      return this;
    }

    withPersistent(persistent: boolean): Builder {
      this.persistent = persistent;
      return this;
    }

    withErrors(errors: Set<Error>): Builder {
      this.errors = errors;
      return this;
    }

    withTriggeredAt(triggeredAt: OrderedSet<Moment>): Builder {
      this.triggeredAt = triggeredAt;
      return this;
    }

    withEnabled(enabled: boolean): Builder {
      this.enabled = enabled;
      return this;
    }

    withReady(ready: boolean): Builder {
      this.ready = ready;
      return this;
    }

    build(): Alert {
      return new Alert(this);
    }

  }

}
