import { Arrays } from '@shared/models/arrays.model';
import { is, ValueObject } from 'immutable';

export class Target implements ValueObject {

  readonly price: number;
  readonly position: Target.Position;

  constructor(builder: Target.Builder) {
    this.price = builder.price;
    this.position = builder.position;
  }

  public static newBuilder(): Target.Builder {
    return new Target.Builder();
  }

  public static copy(target: Target): Target.Builder {
    return Target.newBuilder()
      .withPrice(target.price)
      .withPosition(target.position);
  }

  public static fromJson(json: any): Target {
    return Target.newBuilder()
      .withPrice(json.price)
      .withPosition(Target.Position[<string>json.position])
      .build();
  }

  public equals(other: any): boolean {
    if (!(other instanceof Target))
      return false;

    return is(this.price, other.price) &&
      is(this.position, other.position);
  }

  public hashCode(): number {
    return Arrays.hashCode(this.price, this.position);
  }
}

export namespace Target {

  export enum Position {
    ABOVE = 'ABOVE',
    BELOW = 'BELOW'
  }

  export class Builder {

    price: number;
    position: Target.Position;

    withPrice(price: number): Builder {
      this.price = price;
      return this;
    }

    withPosition(position: Target.Position): Builder {
      this.position = position;
      return this;
    }

    build(): Target {
      return new Target(this);
    }

  }
}
