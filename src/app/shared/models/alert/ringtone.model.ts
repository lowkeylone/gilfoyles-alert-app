import { hash, is, ValueObject } from 'immutable';

export class Ringtone implements ValueObject {

  readonly id: string;
  readonly name: string;
  readonly audio: string;

  constructor(builder: Ringtone.Builder) {
    this.id = builder.id;
    this.name = builder.name;
    this.audio = builder.audio;
  }

  public static newBuilder(): Ringtone.Builder {
    return new Ringtone.Builder();
  }

  public static copy(ringtone: Ringtone): Ringtone.Builder {
    return Ringtone.newBuilder()
      .withId(ringtone.id)
      .withName(ringtone.name)
      .withAudio(ringtone.audio);
  }

  public static fromJson(json: any): Ringtone {
    return Ringtone.newBuilder()
      .withId(json.id)
      .withName(json.name)
      .withAudio(json.audio)
      .build();
  }

  public equals(other: any): boolean {
    if (!(other instanceof Ringtone))
      return false;

    return is(this.id, other.id);
  }

  public hashCode(): number {
    return hash(this.id);
  }
}

export namespace Ringtone {

  export class Builder {

    id: string;
    name: string;
    audio: string;

    withId(id: string): Builder {
      this.id = id;
      return this;
    }

    withName(name: string): Builder {
      this.name = name;
      return this;
    }

    withAudio(audio: string): Builder {
      this.audio = audio;
      return this;
    }

    build(): Ringtone {
      return new Ringtone(this);
    }

  }

}
