import { hash, is, ValueObject } from 'immutable';
import { Subscription } from 'rxjs';

export class WatchProcess implements ValueObject {

  readonly alertId: string;
  readonly subscription: Subscription;

  constructor(builder: WatchProcess.Builder) {
    this.alertId = builder.alertId;
    this.subscription = builder.subscription;
  }

  public static newBuilder(): WatchProcess.Builder {
    return new WatchProcess.Builder();
  }

  public static copy(watchProcess: WatchProcess): WatchProcess.Builder {
    return WatchProcess.newBuilder()
      .withAlertId(watchProcess.alertId)
      .withSubscription(watchProcess.subscription);
  }

  public equals(other: any): boolean {
    if (!(other instanceof WatchProcess))
      return false;

    return is(this.alertId, other.alertId);
  }

  public hashCode(): number {
    return hash(this.alertId);
  }

}

export namespace WatchProcess {

  export class Builder {

    alertId: string;
    subscription: Subscription;

    withAlertId(alertId: string): Builder {
      this.alertId = alertId;
      return this;
    }

    withSubscription(subscription: Subscription): Builder {
      this.subscription = subscription;
      return this;
    }

    build(): WatchProcess {
      return new WatchProcess(this);
    }

  }

}
