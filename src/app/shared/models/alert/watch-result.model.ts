import { Alert } from '@shared/models/alert/alert.model';
import { Arrays } from '@shared/models/arrays.model';
import { is, ValueObject } from 'immutable';

export class WatchResult implements ValueObject {

  readonly alert: Alert;
  readonly triggered: boolean;

  constructor(builder: WatchResult.Builder) {
    this.alert = builder.alert;
    this.triggered = builder.triggered;
  }

  public static newBuilder(): WatchResult.Builder {
    return new WatchResult.Builder();
  }

  public static copy(watchResult: WatchResult): WatchResult.Builder {
    return WatchResult.newBuilder()
      .withAlert(watchResult.alert)
      .withTriggered(watchResult.triggered);
  }

  public equals(other: any): boolean {
    if (!(other instanceof WatchResult))
      return false;

    return is(this.alert, other.alert) &&
      is(this.triggered, other.triggered);
  }

  public hashCode(): number {
    return Arrays.hashCode(this.alert, this.triggered);
  }

}

export namespace WatchResult {

  export class Builder {

    alert: Alert;
    triggered: boolean;

    withAlert(alert: Alert): Builder {
      this.alert = alert;
      return this;
    }

    withTriggered(triggered: boolean): Builder {
      this.triggered = triggered;
      return this;
    }

    build(): WatchResult {
      return new WatchResult(this);
    }

  }

}
