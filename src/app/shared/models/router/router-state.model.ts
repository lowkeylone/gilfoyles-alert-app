export class RouterState {

  readonly url: string;

  private readonly _URL: URL;

  constructor(url: string) {
    this.url = url;
    this._URL = new URL(this.url, window.location.origin);
  }

  URL(): URL {
    return this._URL;
  }
}
