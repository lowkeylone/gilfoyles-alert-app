import { hash, isValueObject } from 'immutable';

export namespace Arrays {

  export function hashCode(...values: any[]) {
    let result = 1;

    values.forEach(value => {
      result = 31 * result + (isValueObject(value) ? value.hashCode() : hash(value));
    });

    return result;
  }

}
