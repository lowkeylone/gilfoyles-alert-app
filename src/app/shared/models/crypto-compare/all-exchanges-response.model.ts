import { Arrays } from '@shared/models/arrays.model';
import { CryptoCompareResponse } from '@shared/models/crypto-compare/response.model';
import { Exchange } from '@shared/models/trading/exchange.model';
import { Pair } from '@shared/models/trading/pair.model';
import { is, Set, ValueObject } from 'immutable';
import * as _ from 'lodash';

export class CryptoCompareAllExchangesResponse implements CryptoCompareResponse, ValueObject {

  readonly response: string;
  readonly hasWarning: boolean;
  readonly type: number;
  readonly message: string;
  readonly data: Set<Exchange>;

  constructor(builder: CryptoCompareAllExchangesResponse.Builder) {
    this.response = builder.response;
    this.message = builder.message;
    this.hasWarning = builder.hasWarning;
    this.type = builder.type;
    this.data = builder.data;
  }

  public static newBuilder(): CryptoCompareAllExchangesResponse.Builder {
    return new CryptoCompareAllExchangesResponse.Builder();
  }

  public static copy(cryptoCompareAllExchanges: CryptoCompareAllExchangesResponse): CryptoCompareAllExchangesResponse.Builder {
    return CryptoCompareAllExchangesResponse.newBuilder()
      .withResponse(cryptoCompareAllExchanges.response)
      .withMessage(cryptoCompareAllExchanges.message)
      .withHasWarning(cryptoCompareAllExchanges.hasWarning)
      .withType(cryptoCompareAllExchanges.type)
      .withData(cryptoCompareAllExchanges.data);
  }

  public static fromJson(json: any): CryptoCompareAllExchangesResponse {
    const exchanges = Set().asMutable();

    _.forIn(json.Data, (value, key) => {
      if (!value.isActive) return;

      const exchange = CryptoCompareAllExchangesResponse.deserializeExchange(key, value);
      exchanges.add(exchange);
    });

    return CryptoCompareAllExchangesResponse.newBuilder()
      .withResponse(json.Response)
      .withMessage(json.Message)
      .withHasWarning(json.HasWarning)
      .withType(json.Type)
      .withData(exchanges.asImmutable())
      .build();
  }

  public equals(other: any): boolean {
    if (!(other instanceof CryptoCompareAllExchangesResponse))
      return false;

    return is(this.response, other.response) &&
      is(this.message, other.message) &&
      is(this.hasWarning, other.hasWarning) &&
      is(this.type, other.type) &&
      is(this.data, other.data);
  }

  public hashCode(): number {
    return Arrays.hashCode(this.response, this.message, this.hasWarning, this.type, this.data);
  }

  private static deserializeExchange(name: string, json: any): Exchange {
    const pairs = Set().asMutable();
    _.mapKeys(json.pairs, (value, key) => CryptoCompareAllExchangesResponse.deserializePair(key, value).forEach(p => pairs.add(p)));

    return Exchange.newBuilder()
      .withName(name)
      .withPairs(pairs.asImmutable())
      .build();
  }

  private static deserializePair(key: string, json: any): Set<Pair> {
    const pairs = Set().asMutable();

    json.forEach(e => {
      pairs.add(Pair.newBuilder()
        .withBaseCurrency(key)
        .withQuoteCurrency(e)
        .build());
    });

    return pairs.asImmutable();
  }
}

export namespace CryptoCompareAllExchangesResponse {

  export class Builder {

    response: string;
    message: string;
    hasWarning: boolean;
    type: number;
    data: Set<Exchange>;

    constructor() {
      this.data = Set();
    }

    withResponse(response: string): Builder {
      this.response = response;
      return this;
    }

    withMessage(message: string): Builder {
      this.message = message;
      return this;
    }

    withHasWarning(hasWarning: boolean): Builder {
      this.hasWarning = hasWarning;
      return this;
    }

    withType(type: number): Builder {
      this.type = type;
      return this;
    }

    withData(data: Set<Exchange>): Builder {
      this.data = data;
      return this;
    }

    build(): CryptoCompareAllExchangesResponse {
      return new CryptoCompareAllExchangesResponse(this);
    }
  }
}
