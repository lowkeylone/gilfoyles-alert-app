export interface CryptoCompareResponse {

  readonly response: string;
  readonly hasWarning: boolean;
  readonly type: number;
}
