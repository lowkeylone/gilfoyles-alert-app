import { Arrays } from '@shared/models/arrays.model';
import { CryptoCompareResponse } from '@shared/models/crypto-compare/response.model';
import { OHLC } from '@shared/models/trading/ohlc.model';
import { is, Set, ValueObject } from 'immutable';

export class HistoricalDataResponse implements CryptoCompareResponse, ValueObject {

  readonly response: string;
  readonly hasWarning: boolean;
  readonly type: number;
  readonly data: Set<OHLC>;

  constructor(builder: HistoricalDataResponse.Builder) {
    this.response = builder.response;
    this.hasWarning = builder.hasWarning;
    this.type = builder.type;
    this.data = builder.data;
  }

  public static newBuilder(): HistoricalDataResponse.Builder {
    return new HistoricalDataResponse.Builder();
  }

  public static copy(historicalDataResponse: HistoricalDataResponse): HistoricalDataResponse.Builder {
    return HistoricalDataResponse.newBuilder()
      .withResponse(historicalDataResponse.response)
      .withHasWarning(historicalDataResponse.hasWarning)
      .withType(historicalDataResponse.type)
      .withData(historicalDataResponse.data);
  }

  public static fromJson(json: any): HistoricalDataResponse {
    return HistoricalDataResponse.newBuilder()
      .withResponse(json.Response)
      .withHasWarning(json.HasWarning)
      .withType(json.Type)
      .withData(Set(json.Data).map(OHLC.fromJson))
      .build();
  }

  public equals(other: any): boolean {
    if (!(other instanceof HistoricalDataResponse))
      return false;

    return is(this.response, other.response) &&
      is(this.hasWarning, other.hasWarning) &&
      is(this.type, other.type) &&
      is(this.data, other.data);
  }

  public hashCode(): number {
    return Arrays.hashCode(this.response, this.hasWarning, this.type, this.data);
  }

}

export namespace HistoricalDataResponse {

  export class Builder {

    response: string;
    hasWarning: boolean;
    type: number;
    data: Set<OHLC>;

    constructor() {
      this.data = Set();
    }

    withResponse(response: string): Builder {
      this.response = response;
      return this;
    }

    withHasWarning(hasWarning: boolean): Builder {
      this.hasWarning = hasWarning;
      return this;
    }

    withType(type: number): Builder {
      this.type = type;
      return this;
    }

    withData(data: Set<OHLC>): Builder {
      this.data = data;
      return this;
    }

    build(): HistoricalDataResponse {
      return new HistoricalDataResponse(this);
    }

  }

}

