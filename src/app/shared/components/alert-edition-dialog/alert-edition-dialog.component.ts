import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { SnackBarService } from '@core/services';
import { AppStore } from '@core/store';
import { AlertsStore } from '@modules/alert/store';
import { Store } from '@ngrx/store';
import { RingtoneCreationDialogComponent } from '@shared/components/ringtone-creation-dialog/ringtone-creation-dialog.component';
import { Alert, Ringtone, SelectOption, Target } from '@shared/models';
import { Set } from 'immutable';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-alert-edition-dialog',
  templateUrl: './alert-edition-dialog.component.html',
  styleUrls: ['./alert-edition-dialog.component.scss']
})
export class AlertEditionDialogComponent implements OnInit {

  formGroup: FormGroup;
  targetPositions: Set<SelectOption<Target.Position>>;
  persistenceOptions: Set<SelectOption<boolean>>;
  ringtones: Set<Ringtone>;

  alertErrors = Alert.Error;

  constructor(private store: Store<AppStore.State>,
              private dialogRef: MatDialogRef<Alert>,
              private formBuilder: FormBuilder,
              private snackBarService: SnackBarService,
              private dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) private data: AlertEditionDialogData) {
    this.targetPositions = Set(Object.keys(Target.Position).map(p => new SelectOption(Target.Position[p], p)));
    this.persistenceOptions = Set.of(new SelectOption(true, 'Every time'), new SelectOption(false, 'Only once'));
    this.ringtones = data.ringtones;
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      exchange: [{value: this.data.alert.exchangeName, disabled: true}],
      pair: [{value: this.data.alert.pair.toString(), disabled: true}],
      position: [this.data.alert.target.position, Validators.required],
      price: [this.data.alert.target.price, Validators.required],
      persistent: [this.persistenceOptions.find(o => o.value === this.data.alert.persistent), Validators.required],
      ringtone: [this.data.ringtones.find(r => r.id === this.data.alert.ringtoneId), Validators.required]
    });
  }

  openRingtoneCreationDialog() {
    this.dialog.open(RingtoneCreationDialogComponent).afterClosed()
      .pipe(filter(Boolean))
      .subscribe(ringtone => {
        this.ringtones = this.ringtones.add(ringtone);
        this.formGroup.controls['ringtone'].setValue(ringtone);
      });
  }

  delete() {
    this.store.dispatch(new AlertsStore.Action.Delete({alert: this.data.alert}));
    this.dialogRef.close();
  }

  submit() {
    if (this.formGroup.invalid)
      return;

    const updatedTarget = Target.copy(this.data.alert.target)
      .withPosition(this.formGroup.controls['position'].value)
      .withPrice(this.formGroup.controls['price'].value)
      .build();

    const persistentOption = this.formGroup.controls['persistent'].value;

    const updatedAlert = Alert.copy(this.data.alert)
      .withTarget(updatedTarget)
      .withPersistent(persistentOption.value)
      .withRingtoneId(this.formGroup.controls['ringtone'].value.id)
      .withReady(false)
      .build();

    this.store.dispatch(new AlertsStore.Action.Update({alert: updatedAlert}));
    this.snackBarService.show('Alert successfully updated.');
    this.dialogRef.close(updatedAlert);
  }
}

export interface AlertEditionDialogData {
  alert: Alert;
  ringtones: Set<Ringtone>;
}
