import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { AppStore } from '@core/store';
import { RingtonesStore } from '@modules/alert/store';
import { Store } from '@ngrx/store';
import { Ringtone } from '@shared/models';
import * as uuid from 'uuid';

@Component({
  selector: 'app-ringtone-creation-dialog',
  templateUrl: './ringtone-creation-dialog.component.html',
  styleUrls: ['./ringtone-creation-dialog.component.scss']
})
export class RingtoneCreationDialogComponent implements OnInit {

  formGroup: FormGroup;

  constructor(private store: Store<AppStore.State>,
              private dialogRef: MatDialogRef<Ringtone>,
              private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      name: [null, Validators.required],
      audio: [null, Validators.required]
    });
  }

  onFileInput(event) {
    const file = event.target.files.length > 0 ? event.target.files[0] : undefined;

    if (!file || !this.isFileSupported(file)) {
      this.formGroup.controls['audio'].setValue(null);
      return;
    }

    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.formGroup.controls['audio'].setValue(e.target.result);
    };
    reader.readAsDataURL(file);
  }

  add() {
    if (this.formGroup.invalid)
      return;

    const ringtone = Ringtone.newBuilder()
      .withId(uuid.v4())
      .withName(this.formGroup.controls['name'].value)
      .withAudio(this.formGroup.controls['audio'].value)
      .build();

    this.store.dispatch(new RingtonesStore.Action.Add({ringtone: ringtone}));
    this.dialogRef.close(ringtone);
  }

  private isFileSupported(file: File): boolean {
    return file.type === 'audio/mp3' || file.type === 'audio/mpeg';
  }
}
