import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewComponent } from '@app/modules/view/containers';

const routes: Routes = [
  {
    path: '',
    component: ViewComponent,
    children: [{
      path: '',
      loadChildren: './modules/alert/alert.module#AlertModule'
    }]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class RoutingModule {}
