export class AppProperties {

  public static readonly APP_NAME = 'gilfoyles-alert-app';

  public static readonly URL = {
    CRYPTOCOMPARE_API: 'https://min-api.cryptocompare.com'
  };
}
