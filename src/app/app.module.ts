import { NgModule } from '@angular/core';
import { AppComponent } from '@app/app.component';
import { RoutingModule } from '@app/routing.module';
import { CoreModule } from '@core/core.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule.forRoot(),
    RoutingModule
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
