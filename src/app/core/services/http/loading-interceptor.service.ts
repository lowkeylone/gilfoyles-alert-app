import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppStore } from '@core/store/app.store';
import { LoadingStore } from '@core/store/loading-store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Injectable()
export class LoadingInterceptorService implements HttpInterceptor {

  constructor(private store: Store<AppStore.State>) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.store.dispatch(new LoadingStore.Action.LoadingStarted());

    return next.handle(req).pipe(
      finalize(() => setTimeout(() => this.store.dispatch(new LoadingStore.Action.LoadingEnded()), 800))
    );
  }

}
