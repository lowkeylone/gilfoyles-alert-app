import { Alert } from '@shared/models';
import { OrderedSet } from 'immutable';

export class StorageService {

  public static storedAlerts(): OrderedSet<Alert> {
    return OrderedSet(JSON.parse(localStorage.getItem('alerts') || '[]'))
      .map(Alert.fromJson);
  }

  public static storeAlert(alert: Alert) {
    StorageService.storeAlerts(OrderedSet.of(alert).merge(StorageService.storedAlerts()));
  }

  public static updateAlert(alert: Alert) {
    StorageService.storeAlerts(StorageService.storedAlerts().add(alert));
  }

  public static removeAlert(alert: Alert) {
    StorageService.storeAlerts(StorageService.storedAlerts().remove(alert));
  }

  private static storeAlerts(alerts: OrderedSet<Alert>) {
    localStorage.setItem('alerts', JSON.stringify(alerts.toArray()));
  }
}
