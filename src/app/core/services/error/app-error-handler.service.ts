import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { SnackBarService } from '@core/services/snackbar/snackbar.service';

@Injectable()
export class AppErrorHandler implements ErrorHandler {

  constructor(private injector: Injector) {}

  handleError(error: any): void {
    try {
      const snackBarService = this.injector.get(SnackBarService);

      snackBarService.showError(error);
    } catch (e) {
      error = e;
    }

    console.error(error);
  }

}
