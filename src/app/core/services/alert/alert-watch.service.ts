import { Injectable } from '@angular/core';
import { CryptoCompareService } from '@core/services/crypto-compare/crypto-compare.service';
import { AppStore } from '@core/store';
import { Store } from '@ngrx/store';
import { Alert, HistoricalDataResponse, WatchProcess, WatchResult } from '@shared/models';
import * as moment from 'moment';
import { Moment } from 'moment';
import { Observable, timer } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

@Injectable()
export class AlertWatchService {

  private static readonly MONITORING_INTERVAL = 15_000;

  constructor(private store: Store<AppStore.State>, private cryptoCompareService: CryptoCompareService) {}

  createWatchProcess(alertId: string, callback: (store: Store<AppStore.State>, result: WatchResult) => void): WatchProcess {
    const startDate = moment.utc();

    const subscription = timer(0, AlertWatchService.MONITORING_INTERVAL).pipe(
      switchMap(() => this.watch(alertId, startDate).pipe(filter(Boolean)))
    ).subscribe((result: WatchResult) => callback(this.store, result));

    return WatchProcess.newBuilder()
      .withAlertId(alertId)
      .withSubscription(subscription)
      .build();
  }

  private watch(alertId: string, startDate: Moment): Observable<WatchResult> {
    const alert$: Observable<Alert> = this.store.select(state => state.alert.alerts.entities.filter(a => a.id === alertId).first()).pipe(filter(Boolean));

    return alert$.pipe(switchMap(alert => this.cryptoCompareService.minuteHistoricalData(alert.pair, alert.exchangeName, 5).pipe(
        map((response: HistoricalDataResponse) => response.data),
        map(data => data.filter(ohlc => ohlc.date.isAfter(startDate))),
        map(data => data.filter(ohlc => alert.triggeredAt.isEmpty() || ohlc.date.isAfter(alert.triggeredAt.last(undefined)!))),
        map(data => {
          let triggered = false;

          data.filter(ohlc => alert.target.price <= ohlc.high)
            .filter(ohlc => alert.target.price >= ohlc.low)
            .forEach(() => triggered = true);

          return WatchResult.newBuilder()
            .withAlert(alert)
            .withTriggered(triggered)
            .build();
        }))));
  }

}
