import { Injectable } from '@angular/core';
import { CryptoCompareService } from '@core/services/crypto-compare/crypto-compare.service';
import { AppStore } from '@core/store';
import { Store } from '@ngrx/store';
import { LivePrice } from '@shared/models';
import { Observable, timer } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

@Injectable()
export class AlertPriceWatchService {

  private static readonly MONITORING_INTERVAL = 15_000;

  constructor(private store: Store<AppStore.State>, private cryptoCompareService: CryptoCompareService) {}

  createWatchProcess(source: LivePrice.Source, callback: (store: Store<AppStore.State>, source: LivePrice.Source, price: number) => void): LivePrice {
    const subscription = timer(0, AlertPriceWatchService.MONITORING_INTERVAL).pipe(
      switchMap(() => this.watch(source).pipe(filter(Boolean)))
    ).subscribe((price: number) => callback(this.store, source, price));

    return LivePrice.newBuilder()
      .withSource(source)
      .withSubscription(subscription)
      .build();
  }

  private watch(source: LivePrice.Source): Observable<number> {
    return this.cryptoCompareService.singleSymbolPrice(source.pair, source.exchangeName).pipe(
      map(response => response.get(source.pair.quoteCurrency, 0)));
  }

}
