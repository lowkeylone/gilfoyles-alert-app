import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppProperties } from '@app/app.properties';
import { AppStore } from '@core/store/app.store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';

@Injectable()
export class CryptoCompareInterceptorService implements HttpInterceptor {

  readonly apiKey$: Observable<string | undefined>;

  constructor(private store: Store<AppStore.State>) {
    this.apiKey$ = this.store.select(state => state.auth.apiKey);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes(AppProperties.URL.CRYPTOCOMPARE_API)) {
      return this.apiKey$.pipe(flatMap(apiKey => {
        const authReq =  req.clone({
          params: req.params.set('api_key', apiKey || '')
        });

        return next.handle(authReq);
      }));
    }

    return next.handle(req);
  }
}
