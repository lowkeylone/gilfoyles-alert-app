import { Action as StoreAction } from '@ngrx/store';

export namespace LoadingStore {

  export interface State {
    inProgress: boolean;
    counter: number;
  }

  export const initialState: State = {
    inProgress: false,
    counter: 0
  };

  export type Actions =
    | Action.LoadingStarted
    | Action.LoadingEnded;

  export namespace Action {

    export enum Type {
      LOADING_STARTED = '[Loading] started',
      LOADING_ENDED = '[Loading] ended'
    }

    export class LoadingStarted implements StoreAction {
      readonly type = Type.LOADING_STARTED;
    }

    export class LoadingEnded implements StoreAction {
      readonly type = Type.LOADING_ENDED;
    }

  }

  export function reducer(state = initialState, action: Actions): State {
    switch (action.type) {
      case Action.Type.LOADING_STARTED: {
        return {
          ...state,
          counter: state.counter + 1,
          inProgress: true
        };
      }

      case Action.Type.LOADING_ENDED: {
        return {
          ...state,
          counter: Math.max(0, state.counter - 1),
          inProgress: (state.counter - 1) > 0
        };
      }

      default:
        return state;
    }
  }

}
