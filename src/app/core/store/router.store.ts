import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { NavigationExtras, Router, RouterStateSnapshot } from '@angular/router';
import { Actions as StoreActions, Effect, ofType } from '@ngrx/effects';
import {
  ROUTER_CANCEL,
  ROUTER_ERROR,
  ROUTER_NAVIGATION,
  RouterAction,
  RouterReducerState,
  RouterStateSerializer
} from '@ngrx/router-store';
import { Action as StoreAction } from '@ngrx/store';
import { RouterState } from '@shared/models';
import { tap } from 'rxjs/internal/operators/tap';
import { map } from 'rxjs/operators';

export namespace RouterStore {

  export interface State extends RouterReducerState<RouterState> {}

  export const initialState: State = {
    state: new RouterState('/'),
    navigationId: -1
  };

  export type Actions =
    | Action.Go
    | Action.Back
    | Action.Forward;

  export namespace Action {

    export enum Type {
      GO = '[Router] go',
      BACK = '[Router] back',
      FORWARD = '[Router] forward'
    }

    export class Go implements StoreAction {
      readonly type = Type.GO;

      constructor(public payload: { path: any[]; extras?: NavigationExtras }) {
      }
    }

    export class Back implements StoreAction {
      readonly type = Type.BACK;
    }

    export class Forward implements StoreAction {
      readonly type = Type.FORWARD;
    }
  }

  export function reducer(state = initialState, action: Actions | RouterAction<any, RouterState>): State {
    switch (action.type) {
      case ROUTER_NAVIGATION:
      case ROUTER_ERROR:
      case ROUTER_CANCEL:
        return {
          state: action.payload.routerState,
          navigationId: action.payload.event.id
        };
      default:
        return state;
    }
  }

}

export class AppRouterStateSerializer implements RouterStateSerializer<RouterState> {

  serialize(routerState: RouterStateSnapshot): RouterState {
    return new RouterState(routerState.url);
  }

}

@Injectable()
export class RouterStoreEffects {

  constructor(private actions$: StoreActions, private router: Router, private location: Location) {}

  @Effect({dispatch: false})
  navigate$ = this.actions$.pipe(
    ofType(RouterStore.Action.Type.GO),
    map((action: RouterStore.Action.Go) => action.payload),
    tap(payload => this.router.navigate(payload.path, payload.extras)));

  @Effect({dispatch: false})
  navigateBack$ = this.actions$.pipe(
    ofType(RouterStore.Action.Type.BACK),
    tap(() => this.location.back()));

  @Effect({dispatch: false})
  navigateForward = this.actions$.pipe(
    ofType(RouterStore.Action.Type.FORWARD),
    tap(() => this.location.forward()));
}
