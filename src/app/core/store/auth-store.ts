import { Injectable } from '@angular/core';
import { Actions as StoreActions, Effect, ofType } from '@ngrx/effects';
import { Action as StoreAction } from '@ngrx/store';
import { StoreFailureAction } from '@shared/models';
import { of } from 'rxjs/internal/observable/of';
import { catchError, map } from 'rxjs/operators';

export namespace AuthStore {

  export interface State {
    apiKey?: string;
  }

  export const initialState: State = {
    apiKey: undefined
  };

  export type Actions =
    | Action.UpdateApiKey
    | Action.ApiKeyUpdateSuccess
    | Action.ApiKeyUpdateFailure;

  export namespace Action {

    export enum Type {
      UPDATE_API_KEY = '[Auth] update api key',
      API_KEY_UPDATE_SUCCESS = '[Auth] api key update success',
      API_KEY_UPDATE_FAILURE = '[Auth] api key update failure'
    }

    export class UpdateApiKey implements StoreAction {
      readonly type = Type.UPDATE_API_KEY;

      constructor(public payload: { apiKey: string }) {}
    }

    export class ApiKeyUpdateSuccess implements StoreAction {
      readonly type = Type.API_KEY_UPDATE_SUCCESS;

      constructor(public payload: { apiKey: string }) {}
    }

    export class ApiKeyUpdateFailure extends StoreFailureAction {
      readonly type = Type.API_KEY_UPDATE_FAILURE;
    }

  }

  export function reducer(state = initialState, action: Actions): State {
    switch (action.type) {
      case Action.Type.API_KEY_UPDATE_SUCCESS: {
        return {
          ...state,
          apiKey: action.payload.apiKey
        };
      }

      default:
        return state;
    }
  }

}

@Injectable()
export class AuthStoreEffects {

  constructor(private actions$: StoreActions) {}

  @Effect()
  updateApiKey$ = this.actions$.pipe(
    ofType(AuthStore.Action.Type.UPDATE_API_KEY),
    map((action: AuthStore.Action.UpdateApiKey) => action.payload.apiKey),
    map(apiKey => new AuthStore.Action.ApiKeyUpdateSuccess({apiKey: apiKey})),
    catchError(err => of(new AuthStore.Action.ApiKeyUpdateFailure(err))));
}
