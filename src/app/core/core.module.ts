import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceModule } from '@core/services/service.module';
import { AppRouterStateSerializer, AuthStoreEffects } from '@core/store';
import { AppStore, AppStoreEffects } from '@core/store/app.store';
import { environment } from '@env/environment';
import { AlertCreationFormStoreEffects, AlertsStoreEffects, RingtonesStoreEffects } from '@modules/alert/store';
import { NotificationStoreEffects } from '@modules/view/store/notification.store';
import { ViewModule } from '@modules/view/view.module';
import { EffectsModule } from '@ngrx/effects';
import { RouterStateSerializer } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ServiceModule,
    ViewModule,
    StoreModule.forRoot(AppStore.reducers),
    EffectsModule.forRoot([
      AppStoreEffects,
      AlertCreationFormStoreEffects,
      AlertsStoreEffects,
      AuthStoreEffects,
      NotificationStoreEffects,
      RingtonesStoreEffects
    ]),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production})
  ]
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule)
      throw new Error('CoreModule is already loaded, import it in the AppModule only');
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        {provide: RouterStateSerializer, useClass: AppRouterStateSerializer}
      ]
    };
  }

}
